# TASK MANAGER 

Console application for task list.  
This version is not final, project is in progress.

# DEVELOPER INFO 

NAME: Ushakov Ivan  
E-MAIL: iushakov@tsconsulting.com  
COMPANY: Technoserv Consulting

# SOFTWARE 

* JDK 15.0.1  
* OS Windows  

# HARDWARE 

* RAM 16Gb  
* CPU i5
* HDD 128Gb  


# RUN PROGRAM 

    java -jar ./task-manager-tsk-04.jar

# SCREENSHOTS 

https://drive.google.com/drive/folders/1YqxV7r-LgjQ9O9HXj-Wx4t2NcdRESdrT?usp=sharing
